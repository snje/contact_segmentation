#!/usr/bin/env python

# This program performs segmentation of contact areas for surface roughness estimation
#
# It is divided into roughly two parts, a fast course segmentation that operates
# on image gradients and a slower finer grained segmentation using graph cutting
#
# Be aware that graph cut consumes a lot of memory (roughly 8GB for a 6k x 5k image),
# so one should parallelized with care.
# They can also take quite a while: 1-2 minutes pr image

import numpy as np
import cv2
import matplotlib.pyplot as plt
import sys
import os
import math
import argparse

# Parse command line arguments
parser = argparse.ArgumentParser(description='Segmentation of contact area')
parser.add_argument(
    'path', metavar='N', type=str, nargs=1,
    help='Path to the image that are to be segmented'
)
parser.add_argument(
    '-g', '--graphcut', dest = "graphcut", action = 'store_true',
    default = False, help='Refines segmentation with graph cut technique'
)
parser.add_argument(
    '-v', '--visualize', dest = "viz", action = 'store_true',
    default = False, help='Shows segmentation after completion'
)
parser.add_argument(
    '-r', '--roi', dest = "roi", action = "store_true",
    default = False,
    help = "Removes empty region not covered by the contact surface in provided input image"
)
args = parser.parse_args()

if len(args.path) < 1:
    print "Please provide image"
    sys.exit(-1)

if len(args.path) > 1:
    print "Multiple images provided, defaulting to the first"

# Read input image and convert to RGB color space
input_path = args.path[0]
im = cv2.imread(input_path)
im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
# Convert to Y Cr Cb color space in order to obtain luminance component
ycrcb = cv2.cvtColor(im, cv2.COLOR_RGB2YCR_CB)
# Calculate the ROI of the contact area and valleys
# These are areas with luminance over a certain threshold
if args.roi:
    bin_roi = ycrcb[:, :, 0] > 60
    bin_roi = bin_roi.astype("uint8")
    # Remove potential noise with medianblur
    bin_roi = cv2.medianBlur(bin_roi, 5)
    # Now find the total ROI containing all objects
    contours, _ = cv2.findContours(
        bin_roi, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE
    )
    def total_frame_border(contours):
        if len(contours) == 0:
            return (0, 0, 0, 0)
        xmin = np.inf
        xmax = -np.inf
        ymin = np.inf
        ymax = -np.inf
        for c in contours:
            x = c[:, 0, 0]
            y = c[:, 0, 1]
            xmin = min(xmin, np.min(x))
            xmax = max(xmax, np.max(x))
            ymin = min(ymin, np.min(y))
            ymax = max(ymax, np.max(y))
        return (xmin, xmax, ymin, ymax)

    (xmin, xmax, ymin, ymax) = total_frame_border(contours)
    # Now crop the color and ycrcb images
    im = im[ymin:ymax, xmin:xmax, :]
    ycrcb = ycrcb[ymin:ymax, xmin:xmax, :]

plt.imshow(im)
plt.show()
# Debug function which shows several images simultaneously with shared axis
def plot_shared(im_array, title = ""):
    rows = 2
    cols = int(math.ceil(len(im_array) / float(rows)))
    f, axarr = plt.subplots(rows, cols, sharex=True, sharey = True)

    def access(r, c):
        if cols != 1:
            return axarr[r, c]
        else:
            return axarr[r]

    for i, im in enumerate(im_array):
        r = i % rows
        c = i / rows
#        axarr[r, c].imshow(im)
        access(r, c).imshow(im)
    access(0, 0).set_title(title)

# Debug function which takes a binary image, detects BLOBS and renders all
# in a unqiue, random colors.
def render_seg(seg):
    cont, _ = cv2.findContours(
        seg.astype("uint8"), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE
    )

    color = np.random.randint(0,255,(len(cont),3))
    segim = np.zeros(im.shape, im.dtype)

    for i in xrange(len(cont)):
        cv2.drawContours(segim, cont, i, color[i, :], -1)

    return segim

# Function which calculates the first order derivatives
def gradient_mag(im):
    dx = cv2.Sobel(im, cv2.CV_32F, 1, 0, ksize = 3)
    dy = cv2.Sobel(im, cv2.CV_32F, 0, 1, ksize = 3)
    return dx, dy, np.sqrt(dx * dx + dy * dy)

# Function which removes BLOBs whose width or length is below the given threshold
def filter_area(seg, min_size):
    cont, _ = cv2.findContours(
        seg.astype("uint8"), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE
    )
    res = np.zeros(seg.shape, dtype = "uint8")
    for i in xrange(len(cont)):
        (x, y, w, h) = cv2.boundingRect(cont[i])
        if w > min_size or h > min_size:
            cv2.drawContours(res, cont, i, (255, 255, 255), -1)
    return res

# Now estimate the gradients, since the tip area is almost always in an area
# with a large contrast, the gradients will have a large absolute value.
# Specifically since the contact areas are basically lines aligned with the
# x-axis, the top of the contact area will have a large positve y-derivative
# and the bottom with have a large negative y-derivative
dx, dy, mag = gradient_mag(ycrcb[:, :, 0])
# The top and bottom is found by thresholding
ldy = dy < -75
udy = dy > 120

# The course contact area is found by growing the top and bottom region towards
# eachother. This done via dilation. The top region is grown downwards.
# the bottom is grown upwards.
k = 71
kernel = np.ones((k, 1), dtype = "uint8")
dil_udy = cv2.dilate(udy.astype("uint8"), kernel, anchor = (0, k - 1))
dil_ldy = cv2.dilate(ldy.astype("uint8"), kernel, anchor = (0, 0))
# The union of the grown top and bottom regions are the course contact area
seg = np.bitwise_and(dil_ldy, dil_udy)
# To refine the segmentation we remove all pixels with an intensity below a
# given threshold
white = (ycrcb[:, :, 0] > 80).astype("uint8")
seg = np.bitwise_and(seg, white)

# A bit of morphlogy is applied
# All operations are done, aligned with the y-axis
k = 15
kernel = np.ones((1, k), dtype = "uint8")
# Here we close holes in the found contact areas
seg = cv2.dilate(seg, kernel)
seg = cv2.erode(seg, kernel)
# Here we remove minor binary noise and disconnect smaller BLOBs
seg = cv2.erode(seg, kernel)
seg = filter_area(seg, 350)
seg = cv2.dilate(seg, kernel)
# Finally we remove BLOBs with areas below a given size

# Go to graph cut refinement if specified by user
if args.graphcut:
    # Intialize background and foreground model
    bgdModel = np.zeros((1,65),np.float64)
    fgdModel = np.zeros((1,65),np.float64)
    seg = cv2.dilate(seg, kernel.T)
    bin_mask = seg > 0
    # Segmentation will be performed in horizontal strips covered by the
    # previously found contact area
    for r in xrange(bin_mask.shape[0]):
        bin_mask[r, :] = np.max(bin_mask[r, :])

    # We initalize the mask which specifies what we think is background and
    # foreground
    mask2 = np.zeros(bin_mask.shape, dtype = "uint8")
    mask2[bin_mask] = cv2.GC_PR_FGD
    mask2[~bin_mask] = cv2.GC_BGD
    mask2[white == 0] = cv2.GC_BGD
    # Run the graph cut algorithm
    cv2.grabCut(im, mask2, None, bgdModel, fgdModel, 5, cv2.GC_INIT_WITH_MASK)
    # Obtain a new binary image
    seg = (mask2 == 3).astype("uint8")
    seg = seg * 255

if args.viz:
    segim = render_seg(seg)
    plot_shared([im, segim, seg * 255], "results")
    plt.show()

parent_dir = os.path.dirname(input_path)
parent_dir = parent_dir if parent_dir != "" else "."
output = parent_dir + "/segmentation.png"
cv2.imwrite(output, seg)
