#!/usr/bin/env python

# This program generates width measurements of a segmented contact area image
# It is concerned with obtaining the total contact area versus image area
# as well as sampling the width of each individual line.
#
# Results can be vizualized and exported to csv

import numpy as np
import cv2
import matplotlib.pyplot as plt
import sys
import os
import math
import argparse
import csv

# Parse command line arguments
parser = argparse.ArgumentParser(description='Extracts metrics for a given line segmentated image')
parser.add_argument(
    'path', metavar='N', type=str, nargs=1,
    help='Path to the image that are to be segmented'
)
parser.add_argument(
    '-o', '--output', dest = "output",
    help = 'Specifies path to output CSV file (default := metrics.cv)'
)
parser.add_argument(
    '-v', '--visualize', dest = "viz", action = 'store_true',
    default = False, help='Shows segmentation after completion'
)
args = parser.parse_args()

if len(args.path) < 1:
    print "Please provide image"
    sys.exit(-1)

if len(args.path) > 1:
    print "Multiple images provided, defaulting to the first"

# Load the binary image
input_path = args.path[0]
im = cv2.imread(input_path, 0)

# Estimate the percentile of line area versus
total_area, line_area = im.shape[0] * im.shape[1], np.sum(im > 0)

# Debug function which shows several images simultaneously with shared axis
def plot_shared(im_array, title = ""):
    rows = 2
    cols = int(math.ceil(len(im_array) / float(rows)))
    f, axarr = plt.subplots(rows, cols, sharex=True, sharey = True)

    def access(r, c):
        if cols != 1:
            return axarr[r, c]
        else:
            return axarr[r]

    for i, im in enumerate(im_array):
        r = i % rows
        c = i / rows
#        axarr[r, c].imshow(im)
        access(r, c).imshow(im, interpolation = "nearest")
    access(0, 0).set_title(title)

# Generate a distance image where each contact area pixel is given a value
# Corresponding to how far it is from the lines edge in a minimum euclidian norm
# sense
d = cv2.distanceTransform(im, cv2.cv.CV_DIST_L2, 5)
# Non-max suppression, this removes all object pixels that are not a local
# vertical maximum. This reduces the line to an image width of one or two pxiels
# where each contain a pixel value corresponding to half of the contact area
# width at that given point.
kernel = np.ones((7, 1), dtype = "uint8")
d_max = cv2.dilate(d, kernel)
d_fil = np.copy(d)
d_fil[d != d_max] = 0
# Now we wish to replace the double pixel instances with a single pixel
# containing the true width value. These occur when the width is an even
# number. Ex if width is 14 then two pixels will appear with the value 7.
# We want to replace these with a single pixel iwth the true value, 7.5.
#
# Double areas are detected through an erosion with a vertical kernel of two
# elements. Therefore only the top part of double pixel instances will remain
kernel = np.ones((3, 1), dtype = "uint8")
kernel[2, 0] = 0
d_double = cv2.erode(d_fil, kernel)
d_multiple = cv2.dilate((d_double > 0).astype("uint8"), np.array([[1, 0, 0, 0, 1]], dtype = "uint8").T)
d_final = np.copy(d_fil)
# The double mask is used to removed the top layer of any double pixel lines
d_final[d_double > 0] = 0
# Now we need to adjust the remaining part of any double pixel values
# We detect the bottom part with a similar erosion, except oppositely anchored.
kernel = np.ones((3, 1), dtype = "uint8")
kernel[0, 0] = 0
d_add = cv2.erode(d_fil, kernel)
# We add 0.5 to obatin the true value
d_final[d_add > 0] += 0.5
# Now we adjust all pixel values to obtain the actual width by 2 * r + 1
d_final *= 2
d_final[d_final > 0] += 1
d_final[d_multiple > 0] = 0
# Remove noise through an area wise filtering
d_mask = (d_final > 0).astype("uint8")
cont, _ = cv2.findContours(
    d_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE
)
def area_filter(c, limit):
    (x, y, w, h) = cv2.boundingRect(c)
    #print x, y, w, h, w > limit or h > limit
    return w > limit or h > limit
cont = filter(lambda c: area_filter(c, 40), cont)

for i, c in enumerate(cont):
    a = c.reshape(-1, 2)
    b = np.ascontiguousarray(a).view(np.dtype((np.void, a.dtype.itemsize * a.shape[1])))
    _, idx = np.unique(b, return_index=True)

    uniques = a[idx]
    x = uniques[:, 0]
    order = np.argsort(x)
    cont[i] = uniques[order, :]

# Now we generate a list of lists where each sublist contains the width values
# for each individual contact area
width_samples = map(lambda c: d_final[c[:, 1], c[:, 0]], cont)

# Debug visualization
if args.viz:
    plot_shared([im, d, d_max, d_fil, d_double, d_multiple, d_final])
    #plot_shared([d_final])
    #plt.figure()
    #plt.imshow(d_final)
    count = np.floor(np.max(d_final))
    mask = (d_final > 0).astype("uint8")
    rad_hist = cv2.calcHist(
        [d_final], [0], mask, [count + 1], [1, count + 1]
    )
    plt.figure()
    plt.bar(np.arange(1, count + 2) - 0.25, rad_hist, 0.5, color='r')
    plt.show()

# Determined whether the user specified an output path or whether to choose the
# default (which is <input_dir>/metrics.csv).
parent_dir = os.path.dirname(input_path)
parent_dir = parent_dir if parent_dir != "" else "."
default_output = parent_dir + "/metrics.csv"
output_path = args.output or default_output


# As the final step we write the obtained data the an output CSV file.
with open(output_path, 'w') as csvfile:
    writer = csv.writer(
        csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL
    )
    writer.writerow(["Line Area(px)", "Total Area(px)"])
    writer.writerow([line_area, total_area])
    writer.writerow([])
    writer.writerow([])

    max_length = reduce(lambda x, y: max(x, len(y)), width_samples, 0)
    rows = map(lambda a: [], np.arange(max_length))
    width_samples = sorted(width_samples, key=lambda l: -len(l))
    for width_list in width_samples:
        for i, w in enumerate(width_list):
            rows[i].append(w)
    writer.writerow(["Line Widths(px)"])
    map(writer.writerow, rows)
